# ChangeLog
## 1.3.1
* Fixed error in sql syntax (BIGlong to BIGINT) that were caused by mass replace in eclipse when moving from int to BIGINT sql storage.
* Added a connection test query. This is required as trews host is using an outdated MySQL driver lib so the Connection.isValid() method is not implemented.
* Updated pom to have the MySQL drivers as `<scope>provided</scope>` so that the plugin uses the host provided drivers.
* Changed pom to include the pom.xml, CONTRIBUTING.md, README.md and CHANGELOG.md in the final jar.

***

## 1.3.0
* Changed database to use BigInt instead of int in order to allow for larger coin / crystal counts.
* Changed to version 1.3.0 as the new database schema in not compatible with earlier versions.
* Added commands to easily add, remove, set and reset the balances of players.
    * **/economy give**
        + **Permission:** `economy.give`
        + **Arguments:**
            + Player name or uuid
            + Coins / Crystals
            + Amount to add
        + **Action:** Add the specified amount to the players coins / crystals.
    * **/economy take**
        + **Permission:** `economy.take`
        + **Arguments:**
            + Player name or uuid
            + Coins / Crystals
            + Amount to take
        + **Action:** Remove the specified amount from the players coins / crystals.
    * **/economy set**
        + **Permission:** `economy.set`
        + **Arguments:**
            + Player name or uuid
            + Coins / Crystals
            + Amount to set as (0 - 9223372036854775807)
        + **Action:** Set the specified amount as the players coins / crystals.
    * **/economy reset**
        + **Permission:** `economy.reset`
        + **Arguments:**
            + Player name or uuid
            + Coins / Crystals
        + **Action:** Reset the players coins / crystals.

