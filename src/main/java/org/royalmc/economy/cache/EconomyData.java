package org.royalmc.economy.cache;

import org.royalmc.economy.Economy;

public class EconomyData {
	private Economy economy;
	private String uuid;
	private String playerName;
	private long crystals;
	private long coins;

	private long creationTime = System.currentTimeMillis();

	public EconomyData(Economy economy, String uuid, String playerName, long crystals, long coins) {
		this.economy = economy;
		this.uuid = uuid;
		this.playerName = playerName;
		this.crystals = crystals;
		this.coins = coins;
	}

	/**
	 * Push any changes in this economy data to the storage database
	 */
	public void updateDatabase() {
		economy.getSqlManager().updateUserData(uuid, coins, crystals);
	}

	/**
	 * 
	 * @return The uuid of the player associated with this object.
	 */
	public String getUUID() {
		return uuid;
	}

	/**
	 * @return the playerName
	 */
	public String getPlayerName() {
		return playerName;
	}

	/**
	 * @return the amount of crystals relevant to this Economy data
	 */
	public long getCrystals() {
		return crystals;
	}

	/**
	 * @param crystals
	 *            the amount of crystals relevant to this Economy data
	 */
	public void setCrystals(long crystals) {
		this.crystals = crystals;
	}

	/**
	 * 
	 * @param crystals
	 *            the amount of crystals to add to this Economy Data
	 */
	public void addCrystals(long crystals) {
		this.crystals += crystals;
	}

	/**
	 * 
	 * @param crystals
	 *            the amount of crystals to remove from this Economy Data
	 * @return true if the removal was successful and false if it was not.
	 */
	public boolean removeCrystals(long crystals) {
		if ((this.crystals - crystals) >= 0) {
			this.crystals -= crystals;
			return true;
		}
		return false;
	}

	/**
	 * @return the amount of coins relevant to this Economy data
	 */
	public long getCoins() {
		return coins;
	}

	/**
	 * @param coins
	 *            the amount of coins relevant to this Economy data
	 */
	public void setCoins(long coins) {
		this.coins = coins;
	}

	/**
	 * 
	 * @param coins
	 *            the amount of crystals to add to this Economy Data
	 */
	public void addCoins(long coins) {
		this.coins += coins;
	}

	/**
	 * 
	 * @param coins
	 *            the amount of coins to remove from this Economy Data
	 * @return true if the removal was successful and false if it was not.
	 */
	public boolean removeCoins(long coins) {
		if ((this.coins - coins) >= 0) {
			this.coins -= coins;
			return true;
		}
		return false;
	}

	/**
	 * @return the creationTime
	 */
	public long getCreationTime() {
		return creationTime;
	}
}
