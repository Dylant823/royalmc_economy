package org.royalmc.economy.commands.arguments;

import org.royalmc.economy.Economy;
import org.royalmc.economy.cache.EconomyData;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class SetArgument {

	private Economy plugin;
	private CommandSender sender;
	private String[] args;

	public SetArgument(Economy plugin, CommandSender sender, String[] args) {
		this.plugin = plugin;
		this.sender = sender;
		this.args = args;
	}

	public void execute() {
		if (!(sender.hasPermission("economy.set"))) {
			sender.sendMessage(new ComponentBuilder("You do not have permission to do this!").color(ChatColor.RED).create());
			return;
		}

		if (args.length == 1) {
			sender.sendMessage(new ComponentBuilder("/").color(ChatColor.DARK_GRAY)
					.append("economy set <PLAYERNAME> <COINS/CRYSTALS> <AMOUNT>").color(ChatColor.GOLD)
					.append(" - ").color(ChatColor.GRAY)
					.append("Set the specified amount as the players coins / crystals.").color(ChatColor.YELLOW).create());
			return;
		}

		else {
			if (args.length < 4) {
				sender.sendMessage(new ComponentBuilder("Insufficient arguments!").color(ChatColor.RED).create());
				sender.sendMessage(new ComponentBuilder("/").color(ChatColor.DARK_GRAY)
						.append("economy set <PLAYERNAME> <COINS/CRYSTALS> <AMOUNT>").color(ChatColor.GOLD)
						.append(" - ").color(ChatColor.GRAY)
						.append("Set the specified amount as the players coins / crystals.").color(ChatColor.YELLOW).create());
				return;
			}
			EconomyData ed = plugin.getEconomyDataCache().getPlayerData(args[1]);
			if (ed == null) {
				sender.sendMessage(new ComponentBuilder("Could not find player with name or uuid " + args[1] + "!").color(ChatColor.RED).create());
				return;
			}

			if (args[2].equalsIgnoreCase("coins")) {
				try {
					long amount = Long.parseLong(args[3]);
					if (amount < 0 || amount > 9223372036854775807l) {
						sender.sendMessage(new ComponentBuilder(amount + " is not a valid number" + "!").color(ChatColor.RED).create());
						sender.sendMessage(new ComponentBuilder("Valid numbers are between 0 and 9223372036854775807l!").color(ChatColor.RED).create());
						return;
					}
					ed.setCoins(amount);
					ed.updateDatabase();
					plugin.getPluginMessageSender().sendSetCoins(ed.getUUID(), ed.getPlayerName(), ed.getCoins(), amount, true);
					sender.sendMessage(new ComponentBuilder("Sucesfully set coins of player " + args[1] + " to " + amount + "!").color(ChatColor.GREEN).create());
				} catch (NumberFormatException e) {
					sender.sendMessage(new ComponentBuilder(args[2] + " is not a number" + "!").color(ChatColor.RED).create());
				}
			} else if (args[2].equalsIgnoreCase("crystals")) {
				try {
					long amount = Long.parseLong(args[3]);
					if (amount < 0 || amount > 9223372036854775807l) {
						sender.sendMessage(new ComponentBuilder(amount + " is not a valid number" + "!").color(ChatColor.RED).create());
						sender.sendMessage(new ComponentBuilder("Valid numbers are between 0 and 9223372036854775807l!").color(ChatColor.RED).create());
						return;
					}
					ed.setCrystals(amount);
					ed.updateDatabase();
					plugin.getPluginMessageSender().sendSetCrystals(ed.getUUID(), ed.getPlayerName(), ed.getCrystals(), amount, true);
					sender.sendMessage(new ComponentBuilder("Sucesfully set crystals of player " + args[1] + " to " + amount + "!").color(ChatColor.GREEN).create());
				} catch (NumberFormatException e) {
					sender.sendMessage(new ComponentBuilder(args[2] + " is not a number" + "!").color(ChatColor.RED).create());
				}
			} else {
				sender.sendMessage(new ComponentBuilder(args[2] + " is not a valid input it must be coins or crystals" + "!").color(ChatColor.RED).create());
			}
		}
	}
}
