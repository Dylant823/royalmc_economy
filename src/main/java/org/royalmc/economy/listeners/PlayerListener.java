package org.royalmc.economy.listeners;

import org.royalmc.economy.Economy;

import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.ProxyReloadEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PlayerListener implements Listener {

	private Economy plugin;

	public PlayerListener(Economy plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onPlayerJoinProxy(PostLoginEvent event) {
		plugin.getSqlManager().initUser(event.getPlayer());
	}

	@EventHandler
	public void onPlayerQuitProxy(PlayerDisconnectEvent event) {
		plugin.getEconomyDataCache().getEconomyDataCache().remove(event.getPlayer().getUniqueId());
	}

	@EventHandler
	public void onProxyReload(ProxyReloadEvent event) {
		plugin.getEconomyDataCache().pullAllEconomyData();
	}
}
