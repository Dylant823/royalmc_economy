package org.royalmc.economy.database.sql;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;

import org.royalmc.economy.Economy;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.scheduler.GroupedThreadFactory;

@SuppressWarnings("deprecation")
public class ConnectionPoolManager {

	private final Economy plugin;

	private HikariDataSource dataSource;

	private String hostname;
	private int port;
	private String database;
	private String username;
	private String password;

	private String testQuery;
	private int minimumConnections;
	private int maximumConnections;
	private long connectionTimeout;
	private long maxLifetime;

	public ConnectionPoolManager(Economy plugin) {
		this.plugin = plugin;
		init();
		setupPool();
	}

	private void init() {
		hostname = plugin.config.getString("SQL.MySQL.Host");
		port = plugin.config.getInt("SQL.MySQL.Port");
		database = plugin.config.getString("SQL.MySQL.Database");
		username = plugin.config.getString("SQL.MySQL.Username");
		password = plugin.config.getString("SQL.MySQL.Password");

		testQuery = plugin.config.getString("SQL.Pool.TestQuery");
		minimumConnections = plugin.config.getInt("SQL.Pool.MinimumConnections", 6);
		maximumConnections = plugin.config.getInt("SQL.Pool.MaximumConnections", 12);
		connectionTimeout = plugin.config.getLong("SQL.Pool.ConnectionTimeout", 5000);
		maxLifetime = plugin.config.getLong("SQL.Pool.MaxLifetime", 330000);
	}

	private void setupPool() {
		try {
			HikariConfig config = new HikariConfig();
			config.setPoolName("RoyalMC_Economy_MySQL");
			config.setJdbcUrl("jdbc:mysql://" + hostname + ":" + port + "/" + database);
			config.setUsername(username);
			config.setPassword(password);

			config.setMinimumIdle(minimumConnections);
			config.setMaximumPoolSize(maximumConnections);
			config.setConnectionTimeout(connectionTimeout);
			config.setConnectionTestQuery(testQuery);
			// Note to other devs the maxLifetime should be at least 30 seconds less than any database-level connection timeout.
			config.setMaxLifetime(maxLifetime); // 330000 (5 min 30 seconds) is the default I have set
			config.setThreadFactory(new GroupedThreadFactory(plugin, "RoyalMC Economy"));
			
			config.addDataSourceProperty("cachePrepStmts", true);
			config.addDataSourceProperty("prepStmtCacheSize", 250);
			config.addDataSourceProperty("prepStmtCacheSqlLimit", 2048);
			dataSource = new HikariDataSource(config);
			plugin.getLogger().log(Level.INFO, ChatColor.GREEN + "Successfully connected to database using MySQL!");
		} catch (Exception e) {
			plugin.getLogger().log(Level.SEVERE, ChatColor.translateAlternateColorCodes('&', "&cError connection to database &6THIS SHOULD HAPPEN THE FIRST TIME THE PROXY IS STARTED AFTER THE PLUGIN HAS BEEN INSTALED!"));
			e.printStackTrace();
			plugin.getLogger().log(Level.SEVERE, ChatColor.translateAlternateColorCodes('&', "&cThe proxy will shut down so that the error can be resolved."));
			plugin.getProxy().stop();
		}
	}

	public Connection getConnection() throws SQLException {
		return dataSource.getConnection();
	}

	public void closePool() {
		if (dataSource != null && !dataSource.isClosed()) {
			dataSource.close();
		}
	}
}