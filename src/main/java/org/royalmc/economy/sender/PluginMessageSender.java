package org.royalmc.economy.sender;

import org.royalmc.economy.Economy;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import net.md_5.bungee.Util;
import net.md_5.bungee.api.config.ServerInfo;

public class PluginMessageSender {

	private Economy plugin;

	public PluginMessageSender(Economy plugin) {
		this.plugin = plugin;
	}

	public void sendEconomyData(String server, String uuid, String name, long coins, long crystals) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();

		out.writeUTF("EconomyData");
		out.writeUTF(uuid);
		out.writeUTF(name);
		out.writeLong(coins);
		out.writeLong(crystals);

		plugin.getProxy().getServerInfo(server).sendData("RoyalMC_Economy", out.toByteArray(), true);
	}

	public void sendSetCoins(String uuid, String name, long balance, long amount, boolean success) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();

		out.writeUTF("SetCoins");
		out.writeUTF(uuid);
		out.writeUTF(name);
		out.writeLong(balance);
		out.writeLong(amount);
		out.writeBoolean(success);

		for (ServerInfo si : plugin.getProxy().getServers().values()) {
			si.sendData("RoyalMC_Economy", out.toByteArray(), true);
		}
	}

	public void sendResetCoins(String uuid, String name) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();

		out.writeUTF("ResetCoins");
		out.writeUTF(uuid);
		out.writeUTF(name);

		for (ServerInfo si : plugin.getProxy().getServers().values()) {
			si.sendData("RoyalMC_Economy", out.toByteArray(), true);
		}
	}

	public void sendCreditCoins(String uuid, String name, long balance, long amount) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();

		out.writeUTF("CreditCoins");
		out.writeUTF(uuid);
		out.writeUTF(name);
		out.writeLong(balance);
		out.writeLong(amount);

		for (ServerInfo si : plugin.getProxy().getServers().values()) {
			si.sendData("RoyalMC_Economy", out.toByteArray(), true);
		}
	}

	public void sendDeditCoins(String uuid, String name, long balance, long amount, boolean success) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();

		out.writeUTF("DebitCoins");
		out.writeUTF(uuid);
		out.writeUTF(name);
		out.writeLong(balance);
		out.writeLong(amount);
		out.writeBoolean(success);

		for (ServerInfo si : plugin.getProxy().getServers().values()) {
			si.sendData("RoyalMC_Economy", out.toByteArray(), true);
		}
	}

	public void sendSetCrystals(String uuid, String name, long balance, long amount, boolean success) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();

		out.writeUTF("SetCrystals");
		out.writeUTF(uuid);
		out.writeUTF(name);
		out.writeLong(balance);
		out.writeLong(amount);
		out.writeBoolean(success);

		for (ServerInfo si : plugin.getProxy().getServers().values()) {
			si.sendData("RoyalMC_Economy", out.toByteArray(), true);
		}
	}

	public void sendResetCrystals(String uuid, String name) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();

		out.writeUTF("ResetCrystals");
		out.writeUTF(uuid);
		out.writeUTF(name);

		for (ServerInfo si : plugin.getProxy().getServers().values()) {
			si.sendData("RoyalMC_Economy", out.toByteArray(), true);
		}
	}

	public void sendCreditCrystals(String uuid, String name, long balance, long amount) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();

		out.writeUTF("CreditCrystals");
		out.writeUTF(uuid);
		out.writeUTF(name);
		out.writeLong(balance);
		out.writeLong(amount);

		for (ServerInfo si : plugin.getProxy().getServers().values()) {
			si.sendData("RoyalMC_Economy", out.toByteArray(), true);
		}
	}

	public void sendDeditCrystals(String uuid, String name, long balance, long amount, boolean success) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();

		out.writeUTF("DebitCrystals");
		out.writeUTF(uuid);
		out.writeLong(balance);
		out.writeLong(amount);
		out.writeBoolean(success);

		for (ServerInfo si : plugin.getProxy().getServers().values()) {
			si.sendData("RoyalMC_Economy", out.toByteArray(), true);
		}
	}

	public void sendTopTenCoins(String server) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();

		out.writeUTF("TopTenCoins");
		out.writeUTF(Util.csv(plugin.getEconomyDataCache().getTopTenCrystals()));

		plugin.getProxy().getServerInfo(server).sendData("RoyalMC_Economy", out.toByteArray(), true);
	}

	public void sendTopTenCrystals(String server) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();

		out.writeUTF("TopTenCrystals");
		out.writeUTF(Util.csv(plugin.getEconomyDataCache().getTopTenCrystals()));

		plugin.getProxy().getServerInfo(server).sendData("RoyalMC_Economy", out.toByteArray(), true);
	}
}
