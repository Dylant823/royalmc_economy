# RoyalMC economy

RoyalMC economy is the bungee based implementation of the network economy system for the developers at RoyalMC to use in there plugins. This plugin uses [Maven](https://maven.apache.org/) in order to manage dependencies and for compiling (Maven is owned by and soely supported by apache). In order to compile this plugin you must have Maven installed. You can find instructions on how to do that [here](https://maven.apache.org/install.html). 

# **WARNING:** 
#### This plugin will stop the BungeeCord proxy from starting the first time it is installed! **THIS IS MEANT TO HAPPEN.** In order to allow the proxy to start you must set valid MySQL details in the plugins config.yml. The server will continue to not start and shall display an error message until a valid MySQL database has been provided.

# Compiling?
1. Make sure you have RoyalMC economy locally on your computer.
2. Navigate to the base folder (which contains the pom.xml).
3. Open a console / terminal in that folder.
4. Type `mvn` and the default build I have specified in the pom.xml will be run.
5. Compiled jar will be in `./target/`.
6. The jar that is compiled with the required dependencies will be sufixed with `jar-with-dependencies` unless you plan on providing the dependencies by other methods please use this jar.

__NOTE:__ This instillation method required that maven is installed on your computer, the maven bin is in your path and that your JAVA_HOME environment variable is set.

# Rules
1. Nothing that will crash a Spigot / BungeeCord server
2. Nothing malicious
3. No removing other's work in PRs
4. Nothing that violates GitLab's Terms of Service
5. You may not publish other's work without their explicit permission

# Contributing
1. Clone (`git clone https://gitlab.com/RoyalMC_Devs/RoyalMC_Economy.git`)
3. Make a new package inside org.royalmc.economy with the feature inside of it or edit already present classes as required. It is preferred if you make this new feature within its own branch but this is not a requirement that is set in stone.
4. Make your code
5. Push (`git push -u origin your-feature`)


# Contributing Guidelines

1. Make sure your master branch is up-to-date before creating a branch to develop your feature.
2. 4 spaces / tab indentation.
3. Keep open braces on the same line as a method, like such: `public void foo(String bar) {`, same goes for if, switch, etc.
4. We are using Java 8. Deal with it.
5. Please use standard Java code conventions throughout your code. If you are not aware of what these are then a copy can be obtained [here](http://www.oracle.com/technetwork/java/codeconventions-150003.pdf).
6. Maven guidelines:
    1. All external dependencies will be handled with Maven.
    2. You may add any repository you require for a dependency.
    3. You may add any dependency you require for your feature.
    4. You must add a scope to every dependency.
        1. You use `<scope>provided</scope>` if you don't want your dependency compiled inside the jar.
        2. You use `<scope>compile</scope>` if you want your dependency compiled inside the jar.
    5. We are using maven-assembly-plugin over maven-shade-plugin to compile dependencies inside the jar.
        1. All you need to do to have your dependency compiled or not inside the jar is add a scope. That's all.
        2. If you wish to complain about assembly-plugin and why we should use shade, don't.
    6. Suggested Maven arguments: `clean compile assembly:single install`.
7. If you need an extra resource - eg. config.yml, <feature-name>.yml, add it in the resources folder and include it in the pom.xml build.
8. Make sure your feature actually works before pushing!

# Message channel and subchannels
### PluginMessageChannel
The plugin message channel that we are using is `"RoyalMC_Economy"` if you are planning to communicate with this plugin from a spigot plugin you will need to register this as both an incoming and outgoing channel in your `onEnadle()`. 
### SubChannels
There are currently several SubChannels implemented. These implemented channels are as follows:
1. EconomyData
2. CreditCoins
3. DebitCoins
4. CreditCrystals
5. DebitCrystals
6. TopTenCrystals
7. TopTenCoin

### Channels explained
##### EconomyData
Request the EconomyData of any player on this proxy.

###### **Arguments**
* String - the name of the server.
* String - the players UUID.

###### **Receiver**
The server named in the first string.

###### **Example**
```Java
out.writeUTF("EconomyData");
out.writeUTF("lobby");
out.writeUTF("5eee1da5a94845558255241feab50d8f");
```
###### **Response**
```Java
String playerUUID = in.readUTF();
String playerName = in.readUTF();
long coins = in.readLong();
long tokens = in.readLong();
```

##### CreditCoins
Credit the specified player with the given amount of coins.

###### **Arguments**
* String - the players UUID.
* long - The amount of coins to be credited.

###### **Receiver**
All servers connected to the proxy.

###### **Example**
```Java
out.writeUTF("CreditCoins");
out.writeUTF("5eee1da5a94845558255241feab50d8f");
out.writeLong(amountToBeCredited);
```
###### **Response**
```Java
String playerUUID = in.readUTF();
String playerName = in.readUTF();
long amount = in.readLong();
```

##### DebitCoins
Debit the specified player with the given amount of coins.

###### **Arguments**
* String - the players UUID.
* long - The amount of coins to be debited.

###### **Receiver**
All servers connected to the proxy.

###### **Example**
```Java
out.writeUTF("DebitCoins");
out.writeUTF("5eee1da5a94845558255241feab50d8f");
out.writeLong(amountToBeDebited);
```
###### **Response**
```Java
String playerUUID = in.readUTF();
String playerName = in.readUTF();
long amount = in.readLong();
boolean sucess = in.readBoolean(); // True if the coins were removed and false if the player did not have enough coins to remove the ammount asked for;
```

##### CreditCrystals
Credit the specified player with the given amount of crystals.

###### **Arguments**
* String - the players UUID.
* long - The amount of crystals to be credited.

###### **Receiver**
All servers connected to the proxy.

###### **Example**
```Java
out.writeUTF("CreditCrystals");
out.writeUTF("5eee1da5a94845558255241feab50d8f");
out.writeLong(amountToBeCredited);
```
###### **Response**
```Java
String playerUUID = in.readUTF();
String playerName = in.readUTF();
long amount = in.readLong();
```

##### DebitCrystals
Debit the specified player with the given amount of Crystals.

###### **Arguments**
* String - the players UUID.
* long - The amount of coins to be debited.

###### **Receiver**
All servers connected to the proxy.

###### **Example**
```Java
out.writeUTF("DebitCrystals");
out.writeUTF("5eee1da5a94845558255241feab50d8f");
out.writeLong(amountToBeDebited);
```
###### **Response**
```Java
String playerUUID = in.readUTF();
String playerName = in.readUTF();
long amount = in.readLong();
boolean success = in.readBoolean(); // True if the crystals were removed and false if the player did not have enough crystals to remove the ammount asked for;
```

##### TopTenCrystals
Get the players with the top ten amounts of crystals from the database.

###### **Arguments**
* String - the name of the server.

###### **Receiver**
All servers connected to the proxy.

###### **Example**
```Java
out.writeUTF("TopTenCrystals");
```
###### **Response**
```Java
String topTen = in.readUTF();
```
**NOTE:** This top ten list is formatted as playerName//amount, playerName//amount, playerName//amount ... This will continue for ten iterations. To retrieve in a list you should first split the string by `, ` and then by ` // `. Note that there is one space after the , and one space before and after the // .

##### TopTenCoins
Get the players with the top ten amounts of coins from the database.

###### **Arguments**
* String - the name of the server.

###### **Receiver**
All servers connected to the proxy.

###### **Example**
```Java
out.writeUTF("TopTenCoins");
```
###### **Response**
```Java
String topTen = in.readUTF();
```
**NOTE:** This top ten list is formatted as playerName//amount, playerName//amount, playerName//amount ... This will continue for ten iterations. To retrive in a list you should first split the string by `, ` and then by ` // `. Note that there is one space after the , and one space before and after the // . 

## Version
1.2.5

## Todos
 * Write test classes to ensure future builds are stable with less user intensive testing being required.
 * Rethink method of sending plugin messages so that it is not thread blocking.
 * Add a method to send all the coins / crystals in an ordered manner through plugin messages.
    * This has not been done already as (due to the limited length of plugin messages) it would have to be split across multiple messages and I am contemplation if an implementation using sockets would be better.
